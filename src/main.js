var Vue = require('vue');
var VueRouter = require('vue-router');

var Welcome = require('./components/welcome.vue');
var Create = require('./components/create.vue');
var Join = require('./components/join.vue');
var Drawer = require('./components/drawer.vue');

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        { path: '/', component: Welcome },
        { path: '/boards/:board', name: 'board', component: Drawer, props: true },
        { path: '/join', component: Join, props: true }
    ]
});

var vue = new Vue({
  router: router
}).$mount('#content');

console.log('Hello Blackboard!');
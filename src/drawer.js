/**
 * Responsible for drawing class
 */

var Raphael = require('raphael');

function Drawer() {
    var blackboard = document.getElementById('blackboard');
    this.rect = blackboard.getBoundingClientRect();
    this.paper = Raphael(blackboard, this.rect.width, this.rect.height);
    this.pressed = false;
    this.activePath = null;
    console.log(Raphael);
    // Hooks on events
    window.onresize = this.OnResize.bind(this);
    blackboard.addEventListener("mousemove", this.OnMouseMove.bind(this), false);
    blackboard.addEventListener("mousedown", this.OnMouseDown.bind(this), false);
    blackboard.addEventListener("mouseup", this.OnMouseUp.bind(this), false);
};

Drawer.prototype.constructor = Drawer;

Drawer.prototype.OnMouseDown = function (event) {
    if (event.button == 0) {
        this.pressed = true;
        var x = event.clientX - this.rect.left;
        var y = event.clientY - this.rect.top;
        this.activePath = this.paper.path("M" + x + " " + y).attr({
            stroke: 'cyan',
            'stroke-width': 5,
        });
    }
};

Drawer.prototype.OnMouseUp = function (event) {
    if (event.button == 0) {
        this.pressed = false;
        var path = this.activePath.attr("path");
        path += "Z";
        this.activePath.attr("path", path);
    }
};

Drawer.prototype.OnMouseMove = function (event) {
    if (this.pressed) {
        var x = event.clientX - this.rect.left;
        var y = event.clientY - this.rect.top;
        var path = this.activePath.attr("path");
        path += "L" + x + " " + y + "M" + x + " " + y;
        this.activePath.attr("path", path);
    }
};

Drawer.prototype.OnResize = function (event) {
    var blackboard = document.getElementById('blackboard');
    this.rect = blackboard.getBoundingClientRect();
    this.paper.setSize(this.rect.width, this.rect.height);
};

module.exports = Drawer;